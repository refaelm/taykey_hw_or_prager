import redis
from rq import Worker, Queue, Connection
import os

from conf import config

listen = [config.JOBS_Q_NAME]

redis_url = os.getenv('REDISTOGO_URL', config.REDIS_CONN_STRING)
conn = redis.from_url(redis_url)
q = Queue(connection=conn)


if __name__ == '__main__':
    with Connection(conn):
        worker = Worker(list(map(Queue, listen)))
        worker.work()
