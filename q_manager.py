from random import randint
import time
import pymongo
from pymongo import MongoClient
from rq import Queue

from conf import config
from worker import conn

q = Queue(connection=conn)
MAX_IN_QUEUE = 20

mongo_client = MongoClient(config.MONGODB_CONNECTION_STRING)
flights_collection = mongo_client.get_database(config.MONGODB_NAME).get_collection(config.FLIGHTS_RESULTS_COLLECTION)
jobs_collection = mongo_client.get_database(config.MONGODB_NAME).get_collection(config.JOBS_COLLECTION)


def start_a_flight_task(job_doc):
    """
    @summary: Executing a flight task, actually it's just sleeping and then inserting the flight combat result
    @param job_doc: Document represent a job in the mongodb job collection
    @type job_doc: dict
    """
    flight_id = job_doc['flight_id']
    print "flight id {0} is starting".format(flight_id)

    time.sleep(randint(10, 45))

    job_doc.update({'winning_weapon': job_doc['good_char_weapon']})
    if job_doc.get('_id'):
        del job_doc['_id']

    flights_collection.update({'flight_id': flight_id}, job_doc, upsert=True)


def main():

    while True:
        # While there is a place for new task in the Queue, just for limiting the q size
        if q.count < MAX_IN_QUEUE:

            try:
                # Pulling out job from the job collection
                job = jobs_collection.find({}).sort('_id', pymongo.ASCENDING).limit(1).next()
            except StopIteration:
                pass

            else:
                # Removing doc from jobs collection
                jobs_collection.remove({'_id': job['_id']})

                # Removing _id so it will be able to be serialized
                if job.get('_id'):
                    del job['_id']

                # Sending job to Q
                q.enqueue_call(
                    func=start_a_flight_task, args=(job,), result_ttl=300, timeout=600
                )

        time.sleep(1)


if __name__ == '__main__':
    main()
