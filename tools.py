from conf import config
from conf.config import VALID_ROLES


def is_valid_color(color_name):
    if color_name in config.COLORS:
        return True
    return False


def is_valid_turtles(turtles):
    for turtle in turtles:
        if turtle.lower() not in config.TURTLES_NAMES:
            return False
    return True


def is_valid_role(role):
    if role in VALID_ROLES:
        return True
    return False
