import requests

from conf import config


def cowabunga():
    payload = {'turtle_name': 'leonardo'}
    res = requests.get(config.APP_URL + "/cowabunga", json=payload)
    print 'received: {0}'.format(res.content)


def multi_turtle():
    payload = {'turtle_name': 'leonardo',
               'number_of_prints': 4}
    res = requests.get(config.APP_URL + "/multi_turtle", json=payload)
    print 'received: {0}'.format(res.content)


def turtle_color():
    payload = {'turtle_name': 'leonardo',
               'color': 'blue'}
    res = requests.post(config.APP_URL + "/turtle_color", json=payload)
    print 'received: {0}'.format(res.content)


def favorite_turtle():
    payload = {'fav_turtles': ['leonardo', 'donatello', 'raphael']}
    res = requests.post(config.APP_URL + "/favorite_turtles", json=payload)
    print 'received: {0}'.format(res.content)


def create_character():
    payload = [{'name': 'Michelangelo', 'weapon': 'Nunchaka', 'role': 'good', 'color': 'orange'},
               {'name': 'Raphael', 'weapon': 'sai', 'role': 'good', 'color': 'red'},
               {'name': 'April Onill', 'weapon': 'camera', 'role': 'good', 'color': 'yellow'},
               {'name': 'Bibop', 'weapon': 'gun', 'role': 'bad', 'color': 'NULL'},
               {'name': 'Shredder', 'weapon': 'claws', 'role': 'bad', 'color': 'grey'},
               {'name': 'Krang', 'weapon': 'brain', 'role': 'bad', 'color': 'pink'},
               ]
    res = requests.post(config.APP_URL + "/create_character", json=payload)
    print 'received: {0}'.format(res.content)


def start_a_flight():
    payload = {'n_flights': 3}
    res = requests.put(config.APP_URL + "/start_a_flight", json=payload)
    print 'received: {0}'.format(res.content)

cowabunga()
multi_turtle()
turtle_color()
favorite_turtle()
create_character()
start_a_flight()
